import React from 'react';
import history from './history';
import MobilePokemonDetails from './scenes/mobile/pokemon_details';
import MobilePokemonLists from './scenes/mobile/my_pokemon_list';
import DesktopPokemonLists from './scenes/desktop/my_pokemon_list';

import {
  Switch, Route, withRouter,
  BrowserRouter
} from 'react-router-dom'; 
import { isMobile } from 'react-device-detect';

function App() {
  let route = (
    <BrowserRouter>
      <Switch>
        <Route exact history={history} path="/" component={DesktopPokemonLists}></Route>
      </Switch>
    </BrowserRouter>
  );

  if (isMobile) {
    route = (
      <BrowserRouter>
        <Switch>
          <Route exact history={history} path="/pokemon_details" component={MobilePokemonDetails}></Route>
          <Route exact history={history} path="/" component={MobilePokemonLists}></Route>
        </Switch>
      </BrowserRouter>
    );
  }

  return (
    <>
      {route}
    </>
  );
}

  export default App;
