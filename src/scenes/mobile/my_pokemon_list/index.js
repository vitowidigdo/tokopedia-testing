import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {jsx} from '@emotion/react';

import style from './index.module.css';

const searchValue = 'Tokopedia'

function MyPokemonList(props) {

  const href = (url) => {
    props.history.push(url);
  }

  const [data, setData] = useState([]);

  const getData = async () => {
    try {
  const getPokeData = await axios.get("https://pokeapi.co/api/v2/pokemon?limit=100&offset=200")
      setData(getPokeData.data);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getData()
  }, [])

  return(
    <>
      <div className={style.container}>
          <div className={style.header}>
            <div className={style.seachBox}
              onClick={() => href("/pokemon_details", {
                value: searchValue,
              })
              }>
              <span className={style.ml_3}>My Pokemon Lists</span>
            </div>
          </div>


          <div style={{padding: "0 20px", marginTop: "4.75rem"}}>
            {/* Category */}
            <div style={{marginTop: "20px", paddingBottom: "31px"}}>
              <div style={{display: "flex", flexDirection: "row", justifyContent: "space-between"}}>
                {/* <div>{data.results[0]}</div> */}
              </div>
            </div>
          </div>
        </div>
    </>
  )
}

export default MyPokemonList;