import axios from 'axios'
import _ from 'lodash'
import Cookies from 'js-cookie'

let ENDPOINTS_POKEMON = new Map([
  ['pokemon_ditto', '/pokemon/ditto'],
])

const hosts = new Map([
  ['pokemon', ENDPOINTS_POKEMON]
])

const API = {}

const parseEndpoint = (endpoint, data) => {
  if (data && typeof data.params !== 'undefined') {
    _.map(data.params, (v, k) => {
      const re = new RegExp(':' + k, 'g')
      const res = re.test(endpoint)
      if (res) {
        _.replace(endpoint, re, v)
      }
    })
  }
  return endpoint
}

hosts.forEach((value, host) => {

  let child = {}
  let instance = axios.create({
    baseURL: "https://pokeapi.co/api/v2/",
  })

  instance.interceptors.request.use(function (config) {
    return new Promise((resolve) => {
      // let token = localStorage.getItem('access_token')
      let token = Cookies.get('access_token')
      config.headers['Accept'] = 'application/json'
      config.headers['Content-Type'] = 'application/json'
      if (token) {
        config.headers['Authorization'] = 'Bearer ' + token
      }

      resolve(config)
    })
  }, function (error) {
    console.log('error ', error)
  })

  instance.interceptors.response.use(function (response) {
    return response
  }, function (error) {
    if (error.response.status === 422) {
      let description = []
      for (var key in error.response.data.errors) {
        description = description.concat(error.response.data.errors[key])
      }
      description = description.join(', ')
    } else if (error.response.status === 401) {
      console.log('')
    } else {
      return error
    }
    return error
  })

  value.forEach((endpoint, key) => {
    child[key] = {
      post: (params) => {
        return instance.post(parseEndpoint(endpoint, params), params)
      },
      get: (params) => {
        return instance.get(parseEndpoint(endpoint, params), params)
      },
      put: (params) => {
        return instance.put(parseEndpoint(endpoint, params), params)
      }
    }
  })

  API[host] = child

  return API
})

export default API
